﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenCvSharp;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            OF.DefaultExt = "*.avi";
            OF.Filter = "동영상 파일(*.avi) |*.avi|모든 파일(*.*)|*.*";
            OF.FilterIndex=1;

            if (OF.ShowDialog() == DialogResult.OK)
            {
                string path = OF.FileName;
                initializing(path);
            }
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            prevFrame();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            nextFrame();
        }

        private void setAttribute_Click(object sender, EventArgs e)
        {

        }

        private void movieTrack_Scroll(object sender, EventArgs e)
        {
            setFrame();
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            timer1.Interval = (int)((1000 / this.fps) + 0.5);
            timer1.Start();
        }

        private void stpButton_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            nextFrame();
        }

        private void fpsTxt_TextChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            if (!fpsTxt.Equals(null))
            {
                this.fps = double.Parse(fpsTxt.Text);
            }
        }
    }
}
