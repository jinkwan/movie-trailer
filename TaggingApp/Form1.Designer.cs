﻿using OpenCvSharp;

namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private CvCapture capture;
        private IplImage frame;
        private int numOfFrames;
        private double fps;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.ribbonTab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTab2 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.fpsTxt = new System.Windows.Forms.TextBox();
            this.fpsLabel = new System.Windows.Forms.Label();
            this.durationLabel = new System.Windows.Forms.Label();
            this.formatLabel = new System.Windows.Forms.Label();
            this.fileLabel = new System.Windows.Forms.Label();
            this.durationTxt = new System.Windows.Forms.TextBox();
            this.formTxt = new System.Windows.Forms.TextBox();
            this.fileNameTxt = new System.Windows.Forms.TextBox();
            this.frameTextField = new System.Windows.Forms.TextBox();
            this.stpButton = new C1.Win.C1Input.C1Button();
            this.runButton = new C1.Win.C1Input.C1Button();
            this.movieTrack = new System.Windows.Forms.TrackBar();
            this.nextButton = new C1.Win.C1Input.C1Button();
            this.setAttribute = new C1.Win.C1Input.C1Button();
            this.prevButton = new C1.Win.C1Input.C1Button();
            this.c1PictureBox1 = new C1.Win.C1Input.C1PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ribbonButton2 = new C1.Win.C1Ribbon.RibbonButton();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.movieTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(1299, 150);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab1);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab2);
            this.c1Ribbon1.VisualStyle = C1.Win.C1Ribbon.VisualStyle.Windows7;
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Groups.Add(this.ribbonGroup1);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Tag";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.ribbonButton1);
            this.ribbonGroup1.Name = "ribbonGroup1";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "Open";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Groups.Add(this.ribbonGroup2);
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "Create";
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.ribbonButton2);
            this.ribbonGroup2.Name = "ribbonGroup2";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 150);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.fpsTxt);
            this.splitContainer1.Panel1.Controls.Add(this.fpsLabel);
            this.splitContainer1.Panel1.Controls.Add(this.durationLabel);
            this.splitContainer1.Panel1.Controls.Add(this.formatLabel);
            this.splitContainer1.Panel1.Controls.Add(this.fileLabel);
            this.splitContainer1.Panel1.Controls.Add(this.durationTxt);
            this.splitContainer1.Panel1.Controls.Add(this.formTxt);
            this.splitContainer1.Panel1.Controls.Add(this.fileNameTxt);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.frameTextField);
            this.splitContainer1.Panel2.Controls.Add(this.stpButton);
            this.splitContainer1.Panel2.Controls.Add(this.runButton);
            this.splitContainer1.Panel2.Controls.Add(this.movieTrack);
            this.splitContainer1.Panel2.Controls.Add(this.nextButton);
            this.splitContainer1.Panel2.Controls.Add(this.setAttribute);
            this.splitContainer1.Panel2.Controls.Add(this.prevButton);
            this.splitContainer1.Panel2.Controls.Add(this.c1PictureBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1299, 527);
            this.splitContainer1.SplitterDistance = 433;
            this.splitContainer1.TabIndex = 1;
            // 
            // fpsTxt
            // 
            this.fpsTxt.Location = new System.Drawing.Point(283, 153);
            this.fpsTxt.Name = "fpsTxt";
            this.fpsTxt.Size = new System.Drawing.Size(100, 21);
            this.fpsTxt.TabIndex = 7;
            this.fpsTxt.TextChanged += new System.EventHandler(this.fpsTxt_TextChanged);
            // 
            // fpsLabel
            // 
            this.fpsLabel.AutoSize = true;
            this.fpsLabel.Location = new System.Drawing.Point(283, 137);
            this.fpsLabel.Name = "fpsLabel";
            this.fpsLabel.Size = new System.Drawing.Size(62, 12);
            this.fpsLabel.TabIndex = 6;
            this.fpsLabel.Text = "Framerate";
            // 
            // durationLabel
            // 
            this.durationLabel.AutoSize = true;
            this.durationLabel.Location = new System.Drawing.Point(281, 94);
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(51, 12);
            this.durationLabel.TabIndex = 5;
            this.durationLabel.Text = "Duration";
            // 
            // formatLabel
            // 
            this.formatLabel.AutoSize = true;
            this.formatLabel.Location = new System.Drawing.Point(281, 55);
            this.formatLabel.Name = "formatLabel";
            this.formatLabel.Size = new System.Drawing.Size(44, 12);
            this.formatLabel.TabIndex = 4;
            this.formatLabel.Text = "Format";
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point(281, 13);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(25, 12);
            this.fileLabel.TabIndex = 3;
            this.fileLabel.Text = "File";
            // 
            // durationTxt
            // 
            this.durationTxt.Location = new System.Drawing.Point(283, 109);
            this.durationTxt.Name = "durationTxt";
            this.durationTxt.Size = new System.Drawing.Size(147, 21);
            this.durationTxt.TabIndex = 2;
            // 
            // formTxt
            // 
            this.formTxt.Location = new System.Drawing.Point(283, 70);
            this.formTxt.Name = "formTxt";
            this.formTxt.Size = new System.Drawing.Size(147, 21);
            this.formTxt.TabIndex = 1;
            // 
            // fileNameTxt
            // 
            this.fileNameTxt.Location = new System.Drawing.Point(283, 28);
            this.fileNameTxt.Name = "fileNameTxt";
            this.fileNameTxt.Size = new System.Drawing.Size(147, 21);
            this.fileNameTxt.TabIndex = 0;
            // 
            // frameTextField
            // 
            this.frameTextField.Location = new System.Drawing.Point(538, 352);
            this.frameTextField.Name = "frameTextField";
            this.frameTextField.Size = new System.Drawing.Size(100, 21);
            this.frameTextField.TabIndex = 8;
            // 
            // stpButton
            // 
            this.stpButton.Location = new System.Drawing.Point(401, 352);
            this.stpButton.Name = "stpButton";
            this.stpButton.Size = new System.Drawing.Size(75, 23);
            this.stpButton.TabIndex = 7;
            this.stpButton.Text = "Stop";
            this.stpButton.UseVisualStyleBackColor = true;
            this.stpButton.Click += new System.EventHandler(this.stpButton_Click);
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(221, 352);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 23);
            this.runButton.TabIndex = 6;
            this.runButton.Text = "Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // movieTrack
            // 
            this.movieTrack.Location = new System.Drawing.Point(221, 301);
            this.movieTrack.Name = "movieTrack";
            this.movieTrack.Size = new System.Drawing.Size(418, 45);
            this.movieTrack.TabIndex = 4;
            this.movieTrack.Scroll += new System.EventHandler(this.movieTrack_Scroll);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(564, 412);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 3;
            this.nextButton.Text = "NextFrame";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // setAttribute
            // 
            this.setAttribute.Location = new System.Drawing.Point(401, 412);
            this.setAttribute.Name = "setAttribute";
            this.setAttribute.Size = new System.Drawing.Size(75, 23);
            this.setAttribute.TabIndex = 2;
            this.setAttribute.Text = "Attribute";
            this.setAttribute.UseVisualStyleBackColor = true;
            this.setAttribute.Click += new System.EventHandler(this.setAttribute_Click);
            // 
            // prevButton
            // 
            this.prevButton.Location = new System.Drawing.Point(221, 412);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(75, 23);
            this.prevButton.TabIndex = 1;
            this.prevButton.Text = "PrevFrame";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // c1PictureBox1
            // 
            this.c1PictureBox1.Location = new System.Drawing.Point(221, 28);
            this.c1PictureBox1.Name = "c1PictureBox1";
            this.c1PictureBox1.Size = new System.Drawing.Size(418, 267);
            this.c1PictureBox1.TabIndex = 0;
            this.c1PictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.LargeImage")));
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "Save";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 677);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.c1Ribbon1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.movieTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void setFrameImage(int pos)
        {
            if (-1 < pos && pos < numOfFrames)
            {
                this.capture.SetCaptureProperty(CaptureProperty.PosFrames, pos);
                this.frame = Cv.QueryFrame(capture);
                c1PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                c1PictureBox1.Image = this.frame.ToBitmap();
                this.c1PictureBox1.Show();
                this.movieTrack.Value = pos;
                this.frameTextField.Clear();
                this.frameTextField.AppendText(pos.ToString() + "/" + numOfFrames.ToString());
            }
        }

        private void clearTextField()
        {
            this.fileNameTxt.Clear();
            this.formTxt.Clear();
            this.durationTxt.Clear();
            this.frameTextField.Clear();
            this.fpsTxt.Clear();
        }

        private void setTimeText()
        {
            int sec = numOfFrames / (int)(this.fps + 0.5);
            int hour = sec / 3600;
            int min = sec % 3600;
            sec = min % 60;
            min = min / 60;
            this.durationTxt.AppendText(hour.ToString() + "h" + min.ToString() + "m" + sec.ToString());
        }

        private void initializing(string name)
        {
            clearTextField();
            this.capture = Cv.CreateFileCapture(name);
            this.numOfFrames = capture.FrameCount;
            this.fileNameTxt.AppendText(name);
            this.fps = capture.Fps;
            this.fpsTxt.AppendText(this.fps.ToString());
            setTimeText();            
            this.movieTrack.Value = 0;
            this.movieTrack.SetRange(0, numOfFrames-1);
            setFrameImage(0);
            //정지 버튼 비활성화
        }
        private void prevFrame()
        {
            setFrameImage(capture.PosFrames - 2);
        }
        private void nextFrame()
        {
            setFrameImage(capture.PosFrames);
        }
        private void setFrame()
        {
            setFrameImage(this.movieTrack.Value);
        }
        #endregion

        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label durationLabel;
        private System.Windows.Forms.Label formatLabel;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.TextBox durationTxt;
        private System.Windows.Forms.TextBox formTxt;
        private System.Windows.Forms.TextBox fileNameTxt;
        private C1.Win.C1Input.C1Button nextButton;
        private C1.Win.C1Input.C1Button setAttribute;
        private C1.Win.C1Input.C1Button prevButton;
        private C1.Win.C1Input.C1PictureBox c1PictureBox1;
        private System.Windows.Forms.TrackBar movieTrack;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Input.C1Button runButton;
        private C1.Win.C1Input.C1Button stpButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox frameTextField;
        private System.Windows.Forms.TextBox fpsTxt;
        private System.Windows.Forms.Label fpsLabel;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton2;
    }
}

